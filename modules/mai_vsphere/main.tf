provider "name" { 
    vsphere_server = "${var.vsphere_server}"
    user = "${var.vsphere_user}"
    password = "${var.vsphere_password}"
    allow_unverified_ssl = true
}

data "vsphere_datacenter" "datacenter" {}


data "vsphere_datastore" "datastore" {
  name = "${var.vsphere_datastore}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
  
}

data "vsphere_resource_pool" "pool" {}

data "vsphere_network" "network" {
  name = "${var.vsphere_network}"
  datacenter_id = "${data.vsphere_datacenter.datacenter.id}"
}

data "vsphere_host" "host" {}

