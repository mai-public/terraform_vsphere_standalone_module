variable "vsphere_server" {
  description = "Server where vsphere is running"
  default = ""
}

variable "vsphere_user" {
  description = ""
  default = ""
}
variable "vsphere_password" {
  description = ""
  default = ""
}
variable "vsphere_datastore" {
  description = ""
  default = ""
}
